# AudioThemes - Plugins - based on OpenColor (open-source color scheme): https://github.com/yeun/open-color

Themes and skins for Linux plugins

Spectacle theme goes in ~/.config/Spectacle/themes/

UhhyouPlugins theme goes in ~/.config/UhhyouPlugins/style/

Stochas theme goes in ~/AudioVitamins/

# Spectacle
![Screenshot of Spectacle](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots/Spectacle.png "Spectacle Screenshot")

# UhhyouPlugins
![Screenshot of UhhyouPlugins](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots/UhhyouPlugins.png "UhhyouPlugins Screenshot")

# Stochas
![Screenshot of Stochas](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots/Stochas.png "Stochas Screenshot")
