# AudioThemes - Themes and skins for Linux audio applications and plugins - based on OpenColor (open-source color scheme): https://github.com/yeun/open-color

Ardour theme goes in ~/.config/ardour6/themes/

Zrythm theme goes in ~/.local/zrythm/themes/css/ (to activate it you have to go to Settings > UI and change the css file name to theme.css)

Note: This theme will work for alpha. A new theme for beta and stable release is in the working.

Audacity theme goes in ~/.audacity-data/Theme/

Note: This theme will work with 2.x. It's known to be not working correctly with 3.2.x.

Muse theme goes in .../muse-4.0/themes/

# Ardour
![Screenshot of Ardour](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots/Ardour.png "Ardour Screenshot")

# Audacity
![Screenshot of Audacity](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots//Audacity.png "Audacity Screenshot")

# MusE
![Screenshot of MusE](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots//MusE.png "MusE Screenshot")

# Zrythm
![Screenshot of Zrythm](https://gitlab.com/lam604/audiothemes/-/raw/main/screenshots/Zrythm.png "Zrythm Screenshot")
